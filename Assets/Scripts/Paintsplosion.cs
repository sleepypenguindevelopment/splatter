﻿using UnityEngine;
using System.Collections;

public class Paintsplosion : MonoBehaviour {
	
	public GameObject dot;

	Pedestal pedestal;
	
	ParticleSystem.CollisionEvent[] collisionEvents = new ParticleSystem.CollisionEvent[16];

	// Use this for initialization
	void Start () {
		pedestal = FindObjectOfType<Pedestal> ();
		if (pedestal != null) 
			Debug.Log("got a pedestal");
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnParticleCollision(GameObject other) {
		int safeLength = particleSystem.safeCollisionEventSize;
		if (collisionEvents.Length < safeLength)
			collisionEvents = new ParticleSystem.CollisionEvent[safeLength];
		
		int numCollisionEvents = particleSystem.GetCollisionEvents(other, collisionEvents);
		int i = 0;
		while (i < numCollisionEvents) {
			Vector3 pos = collisionEvents[i].intersection;
			Quaternion rot = Quaternion.FromToRotation(Vector3.forward, -collisionEvents[i].normal);
			GameObject newDot = Instantiate(dot, pos, rot) as GameObject;
			newDot.transform.SetParent(pedestal.transform, true);
			i++;
		}
	}
}
