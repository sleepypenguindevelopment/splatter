﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pedestal : MonoBehaviour {

	public GameObject statue;
	public Slider slider;
	
	void Awake () {
		// instantiate the statue and place it on the pedestal
		float statueY = statue.renderer.bounds.size.y/2f + renderer.bounds.size.y/2f + transform.position.y;
		GameObject model = Instantiate (statue, new Vector3(transform.position.x, statueY, transform.position.z), transform.rotation) as GameObject;
		model.transform.SetParent (transform, true);
	}

	void SetRotation() {
		int angle = Mathf.RoundToInt(slider.value - transform.rotation.eulerAngles.y);
		transform.Rotate (0, angle, 0);
	}
}
